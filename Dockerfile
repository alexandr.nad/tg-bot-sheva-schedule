FROM ruby:3.2.2-slim-bullseye

WORKDIR /app
RUN apt update -y && apt install build-essential cron -y

COPY Gemfile .
COPY Gemfile.lock .
RUN bundle install

COPY . .
RUN touch logs/application.log

RUN whenever --update-crontab
ENTRYPOINT ["tail", "-f", "logs/application.log"]