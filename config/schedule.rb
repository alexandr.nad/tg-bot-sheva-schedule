require 'rake'

LESSONS_TIME_UTC = {
  first: '07:00am',
  second: '08:30am',
  third: '10:10am',
  fourth: '11:40am',
  fifth: '01:10pm',
  sixth: '02:40pm',
  seventh: '04:10pm'
}

every :monday, at: LESSONS_TIME_UTC[:fourth] do
  rake "monday:fourth_lesson"
end
every :monday, at: LESSONS_TIME_UTC[:fifth] do
  rake "monday:fifth_lesson"
end
every :monday, at: LESSONS_TIME_UTC[:sixth] do
  rake "monday:sixth_lesson"
end


every :wednesday, at: LESSONS_TIME_UTC[:fifth] do
  rake "wednesday:fifth_lesson"
end
every :wednesday, at: LESSONS_TIME_UTC[:sixth] do
  rake "wednesday:sixth_lesson"
end


every :friday, at: LESSONS_TIME_UTC[:fifth] do
  rake "friday:fifth_lesson"
end
every :friday, at: LESSONS_TIME_UTC[:sixth] do
  rake "friday:sixth_lesson"
end
every :friday, at: LESSONS_TIME_UTC[:seventh] do
  rake "friday:seventh_lesson"
end

