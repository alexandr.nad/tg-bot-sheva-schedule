# frozen_string_literal: true

class ScheduleTemplates
  class << self
    def computing_technology
      <<~TEMPLATE
        Технологій обчислень на кластерах і Грід
        Курченко О.А.

        https://us04web.zoom.us/j/3939550950?pwd=b0M1M00zbGhmelUxQW13dTI5dkQ5UT09
      TEMPLATE
    end

    def scientific_seminar
      <<~TEMPLATE
        Науковий семінар з програмної інженерії 
        Порєв Г.В.

        https://meet.google.com/ivd-jrhf-rfg
      TEMPLATE
    end
  end
end
