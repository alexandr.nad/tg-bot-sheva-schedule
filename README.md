## TG-bot sheva schedule

### Configuration

1. Set your telegram bot token: `./Rakefile:5`
2. Set your telegram private group id: `./Rakefile:6`

### How to run

```bash
docker build -t telegram_bot_sheva_schedule .
docker run -d telegram_bot_sheva_schedule
```

###  How to stop 

```bash
docker stop --signal=SIGKILL <containerID>
```
